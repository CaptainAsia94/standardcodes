# -*- coding: utf-8 -*-
"""
Created on Tue Jul 23 13:47:08 2019

@author: kchieng
"""

# Import Libraries 
import sys
import os
import csv
import datetime
import copy
import math
import statistics
import pandas as pd
import numpy as np

import sklearn
from sklearn.preprocessing import Imputer
from sklearn.preprocessing import OneHotEncoder
from sklearn.preprocessing import LabelEncoder


from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.model_selection import KFold
from sklearn.model_selection import cross_val_score
from sklearn.metrics import accuracy_score
from sklearn.metrics import mean_squared_error

import torch
#sys.path.append('D:\\Anaconda3\\envs\\tf_gpu\\Lib\\site-packages')
import tensorflow as tf
import matplotlib.pyplot as plt

# models import
from sklearn import linear_model 
from sklearn.linear_model import LogisticRegression

from sklearn.pipeline import Pipeline
from sklearn.preprocessing import Normalizer
from sklearn.preprocessing import StandardScaler
from sklearn.svm import LinearSVC
from sklearn.preprocessing import PolynomialFeatures

from sklearn.ensemble import RandomForestClassifier

torch.cuda.is_available()

#### Functions
def cat_treatment(data, catcols = 'all', onehotencoding = False, onehotmaxcats = 100):
    
    #Identify categorical columns
    if catcols == 'all':
        catcols = list(data.select_dtypes(include = ['object']).columns)

    # Copy categorical dataset for manipulation
    cat = copy.deepcopy(data[catcols])
    
    ## Cycle through categorical data 
    # Update categorical dictionary throughout 
    catdict = {}     
    for col in catcols:
        
        # Standard Encoding of categories to integer numbers & save categories of each feature
        cat_encoded, categories = cat[col].factorize()       
        catdict[col] = categories

        # Apply one hotencoding if number of categories are not too high or too low (1)        
        if onehotencoding and len(categories) <= onehotmaxcats and len(categories) > 1:
            print("Apply one hot encoding for {}".format(col))
            encoder = OneHotEncoder()
            cat_1hot_sparse = encoder.fit_transform(cat_encoded.reshape(-1,1))
            cat_1hot = cat_1hot_sparse.toarray()
            
            cat = cat.drop([col] , axis = 1).join(pd.DataFrame(cat_1hot
                                       , index = cat.index
                                       , columns = list(col + 'is' + categories))
                                    , rsuffix = '_ind'
                                    )
            
        else:
            print("Apply standard encoding for {}".format(col))
            cat[col] = cat_encoded

    # Re-join data to non-categorical data in return statement
    return {'fulldata': data.drop(catcols , axis = 1).join(cat, rsuffix = '_cat')
            , 'catdata': cat
            , 'categories': catdict
            }


#To log past models
def LogModel(Name, MetricName = np.nan, MetricResult = np.nan, ModelObj = np.nan, Comment = np.nan, InDir = os.getcwd()):
    
    outputd = dict()
    
    graveyard = BuildFolder('Model Graveyard', InDir)
    
    path = InDir + '\\Model Graveyard' + "\\Log.csv"
    outputd['Logpath'] = path
    
    with open (path,'a', newline='') as filedata:                            
      writer = csv.writer(filedata, delimiter=',')

      if os.stat(path).st_size == 0:
          header = ['ID', 'Model Name', 'Metric Name', 'Metric Result', 'Comments' ,'Time Logged', 'Model Location']
          writer.writerow(header)
          LogID = 1
          print("Writing new log")
      else:
          LogID = max(pd.read_csv(path).ID) + 1
      
      writer.writerow([LogID, Name, MetricName, MetricResult, Comment, datetime.datetime.now(), graveyard['path'] + "\\" + Name]) #location WIP
    
    print ( "{} is logged.".format(Name) )
    
    return outputd;

#To build folders
def BuildFolder(FolderName, InDir = os.getcwd()):
    
    outputd = dict()
    outputd['path'] = InDir + '\\' + FolderName
    
    if not os.path.exists(outputd['path']):
        os.makedirs(outputd['path'])
        print("Directory" , FolderName ,  "Created")
    
    return outputd;

#Loss Function
def rmsle(y, y_pred):
                assert len(y) == len(y_pred)
                terms_to_sum = [(math.log(y_pred[i] + 1) - math.log(y[i] + 1)) ** 2.0 for i,pred in enumerate(y_pred)]
                return (sum(terms_to_sum) * (1.0/len(y))) ** 0.5;

# To find error lines
def ErrorRows(f):
    N=len(f)-1
    for i in range(0,N):
        w=f[i].split()
        l1=w[1:8]
        l2=w[8:15]
        try:
            list1=[float(x) for x in l1]
            list2=[float(x) for x in l2]
        except ValueError as e:
            print ("error"+e+"on line"+i)
        result=stats.ttest_ind(list1,list2)
        print (result[1])
    return ;

# load data
def load_data(file , InDir = os.getcwd(), Intype = 'csv', delimiter = ','):
    
    if type(file) == list:
        x = {file[0]: load_data(file[0] , InDir = InDir, Intype = Intype , delimiter = delimiter ) }
        
        if len(file) == 1:
            return x
        
        y = load_data(file[1:] , InDir = InDir, Intype = Intype , delimiter = delimiter ) 
        return {**x, **y}
    
    path = os.path.join(InDir, file)
    
    if Intype == 'text':
        return pd.read_table(path, delimiter = delimiter)
    else:
        return pd.read_csv(path)

## Split train & test data
def split_train_test(data, test_ratio):
    shuffled_indices = np.random.permutation(len(data))
    test_set_size = int(len(data)*test_ratio)
    test_indices = shuffled_indices[:test_set_size]
    train_indices = shuffled_indices[test_set_size:]
    return data.iloc[train_indices],data.iloc[test_indices]

# Standard Pre-processing (add row IDs)
def pre_proc(data, ProcSetting = 'custom'
             , RowID = True, removeNArows = False, imputeNAwMean = False, imputeNAwLDA = False
             , catencoding = True, catcols = 'all', onehotencoding = False, onehotmaxcats = 100
             , ScaleNorm = False , Normalize = False, norm='l2'
             , showhist = False
             , splittesttrain = True, test_ratio = 0.2
             , file = '', InDir = os.getcwd(), Intype = 'csv', delimiter = ','):
    
    #Pre defined settings
    procsettings = {'standard': {'removeNArows': False, 'RowID': True, 'imputeNAwMean': False, 'imputeNAwLDA': False, 'catencoding':True},
                    'removeNAs': {'removeNArows': True, 'RowID': True, 'imputeNAwMean': False, 'imputeNAwLDA': False},
                    'ImputeMean': {'removeNArows': False, 'RowID': True, 'imputeNAwMean': True, 'imputeNAwLDA': False},
                    'ImputeLDA': {'removeNArows': False, 'RowID': True, 'imputeNAwMean': False, 'imputeNAwLDA': True},
                    'nothing': {'removeNArows': False, 'RowID': False, 'imputeNAwMean': False, 'imputeNAwLDA': False},
                    'juststats': {'removeNArows': False, 'RowID': False, 'showhist': True, 'splittesttrain': False, 'imputeNAwMean': False, 'imputeNAwLDA': False}
                    }
    
    #Load data & pre proc settings
    try:
        pre_data = data
    except NameError: 
        pre_data = load_data(file = file, InDir = InDir, Intype = Intype, delimiter = delimiter)

    if type(pre_data) == dict:
        preprocdict = {}
        
        for k, df in pre_data.items():
            print(k)
            preprocdict[k] = pre_proc(df, ProcSetting , RowID , removeNArows , imputeNAwMean 
                       , imputeNAwLDA, catencoding, catcols, onehotencoding, onehotmaxcats, ScaleNorm , Normalize , norm , showhist 
                       , splittesttrain , test_ratio , file, InDir, Intype , delimiter )
            
        return preprocdict

    if ProcSetting != 'juststats':        
        print('**********RAW DATA DESCRIPTION**********')
        print(pre_data.info())
        print(pre_data.describe())
    
    if ProcSetting in procsettings.keys():       
        locals().update(procsettings[ProcSetting])

    #Begin Pre-processing
    # Add RowID
    if RowID and 'RowID' not in pre_data.columns:
        pre_data['RowID'] = np.arange(len(pre_data))+1

    # NaN treatment    
    if removeNArows:
        pre_data.dropna()
    elif imputeNAwMean:
        pre_data.fillna(pre_data.mean(), inplace=True)
    elif imputeNAwLDA:
        LDA = LinearDiscriminantAnalysis()
        imputer = Imputer()
        transformed_X = imputer.fit_transform(pre_data)
        # evaluate an LDA model on the dataset using k-fold cross validation
        print('Need to test LDA if using this form of imputation. Results of k-fold cross validation is as follows: ')
        model = LinearDiscriminantAnalysis()
        kfold = KFold(n_splits=3, random_state=7)
        result = cross_val_score(model, transformed_X, y, cv=kfold, scoring='accuracy')
        print(result)
        pre_data.fillna(transformed_X, inplace=True) #Imputation LDA still WIP!!

    # Categorical variable treatment
    if catencoding:
        encodingdict = cat_treatment(pre_data, catcols = catcols, onehotencoding = onehotencoding, onehotmaxcats = onehotmaxcats)
        pre_data = encodingdict['fulldata']
       
    if ScaleNorm:
        scaler = StandardScaler()
        scaled_features  = scaler.fit_transform(pre_data.values)
        pre_data = pd.DataFrame(scaled_features, index=pre_data.index, columns=pre_data.columns)

    if Normalize:
        transformer = Normalizer(norm = norm).fit(pre_data)
        transformed_features = transformer.transform(pre_data)
        pre_data = pd.DataFrame(transformed_features , index=pre_data.index, columns=pre_data.columns)
        
    print('**********PROCESSED DATA DESCRIPTION**********')  
    print(pre_data.info())
    print(pre_data.describe())
    if showhist:
        pre_data.hist(bins = 50, figsize = (20,15))
    
    if splittesttrain:
        train_set, test_set = split_train_test(pre_data, test_ratio = test_ratio)
    else:
        train_set = False
        test_set = False
    
    return({'processed': pre_data, 'train': train_set, 'test': test_set, 'categories': encodingdict['categories']})
       
#Gini Function
#a and b are the quantities of each class
def gini(a,b):
    a1 = (a/(a+b))**2
    b1 = (b/(a+b))**2
    return 1 - (a1 + b1)
#Defining Gini info gain function:
def gini_info_gain(pa,pb,c1a,c1b,c2a,c2b):
    return (gini(pa,pb))-((((c1a+c1b)/(pa+pb))*gini(c1a,c1b)) + (((c2a+c2b)/(pa+pb))*gini(c2a,c2b)))

def entropy(base,a,b):
    try:
        var =  abs(((a)/(a+b)) * log(((a)/(a+b)),base)) - (((b)/(a+b)) * log(((b)/(a+b)),base))
        return var
    except (ValueError):
        return 0
#Defining Entropy info gain function:
def entropy_info_gain(base,pa,pb,c1a,c1b,c2a,c2b):
    return (entropy(base,pa,pb))-((((c1a+c1b)/(pa+pb))*entropy(base,c1a,c1b)) + (((c2a+c2b)/(pa+pb))*entropy(base,c2a,c2b)))

# Feature relationship against Response Variable
def featurerelationships(data, y_response = None, x_feature = None , hist = False):
    
    if x_feature == None: 
        x_feature = list(data.columns)
    
    if y_response in x_feature:
        x_feature.remove(y_response)
    
    allattributes = x_feature + [y_response]
    
    corr_matrix = data[allattributes].corr()
    
    if hist:
        data[allattributes].hist(bins = 50, figsize = (20,15))
    
    return({'corr_matrix':corr_matrix
            , "y_response_corr": corr_matrix[y_response].sort_values(ascending = False)
            })

def testrmse(model, TestSet_x, Label_y):   
    predictions = model.predict(TestSet_x)
    mse = mean_squared_error(Label_y,predictions)
    return np.sqrt(mse)

### INPUTS
WorkingDir = r'C:\Users\kchieng\Documents\Projects\191028_xxxVoiceAnalytics\WRK'
RAWDir = r'C:\Users\kchieng\Documents\Projects\191028_xxxVoiceAnalytics\RAW'
RAWData = [r'xxx - Media Set - 20191001-20191004.csv'
           , r'xxx_HITS_AGENT_20190919-20190920.csv'
           ,r'xxx_HITS_CUSTOMER_20190919-20190920.csv'
           , r'xxx_Metadata_20190903-20190909.csv']

### Setup Working Directory
os.chdir(WorkingDir)
BuildFolder('Log',InDir=WorkingDir)

### Load Datasets
FullSet = load_data(RAWData, InDir = RAWDir, Intype = 'csv')
Datasets = pre_proc(FullSet, ProcSetting = 'standard')

WRK_MediaSet = Datasets['xxx - Media Set - 20191001-20191004.csv']['processed']
WRK_HITS_AGENTSet = Datasets['xxx_HITS_AGENT_20190919-20190920.csv']['processed']
WRK_HITS_CUSTOMERSet = Datasets['xxx_HITS_CUSTOMER_20190919-20190920.csv']['processed']
WRK_Metadata_CUSTOMERSet = Datasets['xxx_Metadata_20190903-20190909.csv']['processed']

## Modelling
datastats = pre_proc(FullSet, ProcSetting = 'juststats')

## Columns with sufficient data
MediaSet_col = ['Media File','Media Container','Created On','Discovered On','Duplicate %'
                ,'Duplicate Duration','Duplicate Duration (sec)','Duplicate Occurrences'
                ,'Duration','Duration (sec)','Encrypted','File Extension','File Name'
                ,'Indexed Languages','Last Modified','Reviewed']

Meta_col = ['CallTypeCode','OrganisationCode','MediaCode','Campaign','AgentId','Agent_Name'
            ,'ConsolidatedTeam','PackageCode','Start','Contact','IsRightPartyContact'
            ,'WrapUpCategory','WrapUpCode','CallDuration','CallsHandled','InteractionIDKey'
            ,'recordingid','Sale Activity']

DRV_Metadata_CUSTOMERSet = pre_proc(WRK_Metadata_CUSTOMERSet[Meta_col], RowID = False, imputeNAwMean = True, catencoding = False
                                    , onehotencoding = True, ScaleNorm = True, splittesttrain = False)

## Initial Observations
Mediarel = featurerelationships(WRK_MediaSet[MediaSet_col], y_response = 'Duration') #insufficient info
Mediarel['corr_matrix']
# Duration distributed exponentially (expected)
# Insufficient data for most columns (low correlation between features)

HITS_AGENTrel = featurerelationships(WRK_HITS_AGENTSet, y_response = 'Score') 
HITS_AGENTrel['corr_matrix']
# High correlation between Start Offset (ms) & Term (0.391257) - need to know what this data is used for
# High correlation between Start Offset (ms) & Displayed Phrase (0.391664) - need to know what this data is used for

HITS_CUSTOMERrel = featurerelationships(WRK_HITS_CUSTOMERSet, y_response = 'Score') 
HITS_CUSTOMERrel['corr_matrix']
# same as HITS_AGENTrel

Metadatarel = featurerelationships(WRK_Metadata_CUSTOMERSet[Meta_col], y_response = 'Sale Activity', hist = False) 
Metadatarel['corr_matrix']
Metadatarel['y_response_corr']

TrainSet = Datasets['xxx_Metadata_20190903-20190909.csv']['train']
TestSet = Datasets['xxx_Metadata_20190903-20190909.csv']['test']

Y = 'Sale Activity'
Xs = copy.deepcopy(Meta_col)
Xs.remove(Y)

TrainSet_x = TrainSet[Xs]
TestSet_x = TestSet[Xs]

TrainSet_x.fillna(TrainSet_x.mean(), inplace=True)
TestSet_x.fillna(TrainSet_x.mean(), inplace=True)

TrainSet_y = TrainSet[Y]
TestSet_y = TestSet[Y]

### Model Fitting

## OLS Linear Regression
reg_OLS = linear_model.LinearRegression()
reg_OLS.fit(TrainSet_x,TrainSet_y)

# Test
reg_OLSrmse = testrmse(reg_OLS, TestSet_x, TestSet_y) 
#0.1774704080178799
        
## Logistic Regression
log_reg = LogisticRegression()
log_reg.fit(TrainSet_x,TrainSet_y)

# Test
log_regrmse = testrmse(log_reg, TestSet_x, TestSet_y) 
#0.2054150380516849

## (linear) support vector machine 
svm_clf = Pipeline([
            ("scaler", StandardScaler()),
            ("linear_svc", LinearSVC(C=1, loss = "hinge"))
        ])
svm_clf.fit(TrainSet_x,TrainSet_y)

# Test
svm_clfrmse = testrmse(svm_clf, TestSet_x, TestSet_y) 
#0.1250553138164103

## (non-linear) support vector machine 
polynomial_svm_clf = Pipeline([
            ("poly_features", PolynomialFeatures(degree = 3))
            ("scaler", StandardScaler()),
            ("svm_clf", LinearSVC(C=10, loss = "hinge"))
        ])
polynomial_svm_clf.fit(TrainSet_x,TrainSet_y)
# TypeError: 'tuple' object is not callable

## support vector machine (+kernel) 

## Random Forest (LOWEST RMSE)
rnd_clf = RandomForestClassifier(n_estimators = 500, max_leaf_nodes = 16, n_jobs = -1)
rnd_clf.fit(TrainSet_x,TrainSet_y)

# Test
rnd_clfrmse = testrmse(rnd_clf, TestSet_x, TestSet_y) 
#0.0543205571318736

# Visualise
from sklearn.tree import export_graphviz
# Export as dot file
estimator = rnd_clf.estimators_[5]
export_graphviz(estimator, out_file='tree.dot', 
                feature_names = list(TestSet_x.columns),
                class_names = Datasets['xxx_Metadata_20190903-20190909.csv']['categories']['Sale Activity'],
                rounded = True, proportion = False, 
#                precision = 2, 
                filled = True)
from subprocess import call
call(['dot', '-Tpng', 'tree.dot', '-o', 'tree.png', '-Gdpi=600'])


# Convert to png using system command (requires Graphviz)
from subprocess import call
call(['dot', '-Tpng', 'tree.dot', '-o', 'tree.png', '-Gdpi=600'])

# Display in jupyter notebook
from IPython.display import Image
Image(filename = 'tree.png')



### Ensemble Models



### Test against testset



TrainSet.head()
TrainSet.columns
TrainSet.dtypes
TrainSet.info()
TrainSet.head().iloc[:,:81]

TrainSet.iloc[:,:80].head()

TrainSet.corr(TrainSet.iloc[:,:80])

type(TrainSet.PoolQC)
TrainSet.PoolQC[TrainSet.PoolQC.notnull()]

dir(TrainSet)


TestSet.head()

type(TrainSet)

i = 0
for row in TrainSet:
    i += 1
    print ', '.join(row)

        


## Create NA removed dataset
#TrainSet_NA = copy.deepcopy(TrainSet)
#TrainSet_NA.dropna(inplace = True)
#TrainSet_NA.describe() # (Number of rows, number of columns)
#
## Replace with mean
#TrainSet_impmean = copy.deepcopy(TrainSet)
#TrainSet_impmean.fillna(TrainSet_impmean.mean(), inplace=True)
#TrainSet_impmean.describe()
#
## Replace using Linear Descriminant Analysis (LDA)
#TrainSet_impLDA = copy.deepcopy(TrainSet)
#LDA = LinearDiscriminantAnalysis()
#imputer = Imputer()
#transformed_X = imputer.fit_transform(TrainSet_impLDA)
## evaluate an LDA model on the dataset using k-fold cross validation
#model = LinearDiscriminantAnalysis()
#kfold = KFold(n_splits=3, random_state=7)
#result = cross_val_score(model, transformed_X, y, cv=kfold, scoring='accuracy')


### Data Preprocessing 

## Clean Dataset

## Split response variable
TrainSet_y = TrainSet.SalePrice
TrainSet_x = TrainSet.iloc[:,1:80]

## Feature Engineering

## Standardisation

## Dimensionality Reduction
This e-mail and any attachments to it are confidential. You must not use, disclose or act on the e-mail if you are not the intended recipient. If you have received this e-mail in error, please let us know by contacting the sender and deleting the original e-mail. Liability limited by a scheme approved under Professional Standards Legislation. Deloitte refers to a Deloitte member firm, one of its related entities, or Deloitte Touche Tohmatsu Limited (“DTTL”). Each Deloitte member firm is a separate legal entity and a member of DTTL. DTTL does not provide services to clients. Please see www.deloitte.com/about to learn more. Nothing in this e-mail, nor any related attachments or communications or services, have any capacity to bind any other entity under the ‘Deloitte’ network of member firms (including those operating in Australia). 
